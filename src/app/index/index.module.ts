import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { IndexComponent } from './index.component';

/*
 *  Exports the index page component.
 */
@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [
    IndexComponent
  ]
})
export class IndexModule { }
