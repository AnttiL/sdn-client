import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';

import { APP_CONFIG, ApplicationConfig } from '../core/config/app.config';

/*
 *  Index page component.
 */
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})

export class IndexComponent implements OnInit {
  constructor(
    @Inject(APP_CONFIG)
    private config: ApplicationConfig
  ) { }

  ngOnInit() {
  }

}
