import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

/*
 *  Guard for user authentication protected routes.
 */
@Injectable()
export class AuthGuardService {
  constructor(private router: Router) { }

  /*
   *  Returns true if the user is authenticated.
   *  Redirects the user to the login page when trying to access a restricted page while not logged in.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('user')) {
      // Logged in.
      return true;
    }
    // Logged out.
    this.router.navigate(['/login']);
    return false;
  }
}
