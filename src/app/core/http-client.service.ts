import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApplicationConfig, APP_CONFIG, APP_DI_CONFIG } from './config/app.config';

/*
 *  Http client service for sending and handling RESTful API requests.
 */
@Injectable()
export class HttpClientService {

  constructor(
    @Inject(APP_CONFIG)
    private config: ApplicationConfig,
    private http: Http
  ) { }

  /*
   *  Adds the Authorization header to the requests if the 'user' localStorage item is set.
   */
  createAuthorizationHeader(headers: Headers) {
    const user = localStorage.getItem('user');
    if (user) {
      headers.append('Authorization', 'Bearer ' + JSON.parse(user).token);
    }
  }

  /*
   *  GET request.
   *  @param path API request path.
   */
  get(path): Observable<Response>  {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.config.apiEndpoint + path, {
      headers: headers
    }).map((response) => {
      return response || this.handleEmptyResponse;
    }).catch(this.handleError);
  }

  /*
   *  POST request.
   *  @param path API request path.
   *  @param data Payload to send.
   */
  post(path, data): Observable<Response> {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    headers.append('Content-type', 'application/json');
    return this.http.post(this.config.apiEndpoint + path, data, {
      headers: headers
    }).map((response: Response) => {
      return response || this.handleEmptyResponse;
    }).catch(this.handleError);
  }

  /*
   *  PUT request.
   *  @param path API request path.
   *  @param data Payload to send.
   */
  put(path, data): Observable<Response>  {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(this.config.apiEndpoint + path, data, {
      headers: headers
    });
  }

  /*
   *  PATCH request.
   *  @param path API request path.
   *  @param data Payload to send.
   */
  patch(path, data): Observable<Response>  {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.patch(this.config.apiEndpoint + path, data, {
      headers: headers
    });
  }

  /*
   *  DELETE request.
   *  @param path API request path.
   */
  delete(path): Observable<Response>  {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.delete(this.config.apiEndpoint + path, {
      headers: headers
    });
  }

  handleEmptyResponse() {
    return {
      success: false,
      message: 'No response from the API server.'
    };
  }

  handleError(error: Response | any): Observable<any> {
    let errorMessage: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error ||  JSON.stringify(body);
      errorMessage = error.status + ' - ' + error.statusText || '' + err;
    } else {
      errorMessage = error.message ? error.message : error.toString();
    }
    return Observable.throw(errorMessage);
  }
}
