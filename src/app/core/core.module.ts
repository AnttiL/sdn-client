import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { APP_CONFIG, APP_DI_CONFIG } from './config/app.config';
import { AuthGuardService } from './auth-guard.service';
import { HttpClientService } from './http-client.service';
import { TopNavComponent } from './top-nav/top-nav.component';

/*
 *  Exports common modules and components used by other modules.
 *  Also provides the application configuration file.
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpModule
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG
    },
    AuthGuardService,
    HttpClientService
  ],
  declarations: [
    TopNavComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    RouterModule,
    TopNavComponent
  ]
})
export class CoreModule { }
