import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/*
 *  Top navigation bar component.
 */
@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
  loggedIn: boolean;

  constructor(
    private router: Router
  ) {
    // Check if the user is logged in to change the view accordingly.
    if (localStorage.getItem('user')) {
      this.loggedIn = true;
    }
  }

  ngOnInit() { }

  /*
   *  Unsets the local storage user item and redirect the user to the login page.
   */
  LogOut() {
    localStorage.removeItem('user');
    this.router.navigate(['/login']);
  }

}
