import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { FormModule } from '../form/form.module';
import { TopologyService } from './topology.service';

@NgModule({
  imports: [
    CoreModule,
    FormModule
  ],
  providers: [
    TopologyService
  ],
  declarations: []
})
export class TopologyModule { }
