import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import { HttpClientService } from '../core/http-client.service';
import { Endpoint } from '../models/endpoint.model';
import { Switch } from '../models/switch.model';
import { SwitchLink } from '../models/switch-link.model';

/*
 *  Back-end REST API calls.
 */
@Injectable()
export class TopologyService {

  constructor(private http: HttpClientService) { }

  /*
   *  Retrieves switches from the back-end.
   */
  loadSwitches(): Observable<Switch[]> {
    return this.http.get('/api/switches')
      .map((response: Response) => {
        return response.json();
      });
  }

  /*
   *  Retrieves endpoints from the back-end.
   */
  loadEndpoints(): Observable<Endpoint[]> {
    return this.http.get('/api/endpoints')
      .map((response: Response) => {
        return response.json();
      });
  }

  /*
   *  Retrieves links between nodes from the back-end.
   */
  loadLinks(): Observable<any> {
    const switchLinks = this.http.get('/api/links/switches')
      .map((response: Response) => {
        return response.json();
      });
    const endpointLinks = this.http.get('/api/links/endpoints')
      .map((response: Response) => {
        return response.json();
      });
    return Observable.forkJoin([switchLinks, endpointLinks]);
  }

  /*
   *  Retrieves virtual ports from the back-end.
   */
  loadVirtualPorts(): Observable<any> {
    return this.http.get('/api/vports')
      .map((response: Response) => {
        return response.json();
      });
  }
}
