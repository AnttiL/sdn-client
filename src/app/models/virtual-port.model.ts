export class VirtualPort {
  enfError: any;
  enfMirroring: any;
  enfPortConf: any;
  enfPortState: string;
  enfPriority: number;
  enfServiceConf: any;
  enfTraffMatch: any;
  id: string;
  parent: string;
  portId: string;
  reqMirroring: any;
  reqPortConf: any;
  reqPortState: string;
  reqPriority: number;
  reqServiceConf: any;
  reqTraffmatch: any;
  vportNotes: any;
}
