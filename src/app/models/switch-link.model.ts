export class SwitchLink {
  id: string;
  endpointId: string;
  switchPortId: string;
  vportId: string;
}
