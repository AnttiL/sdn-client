export class Switch {
  id: string;
  role: string;
  source: string;
  target: string;
  ports: any;
}
