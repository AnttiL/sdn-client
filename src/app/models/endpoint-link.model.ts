export class EndpointLink {
  id: string;
  endpointId: string;
  switchPortId: string;
  vportId: string;
}
