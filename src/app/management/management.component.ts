import { Component, OnInit } from '@angular/core';
import cytoscape from 'cytoscape';

import { CytoscapeConfig as cs } from '../core/config/cytoscape.config';
import { TopologyService } from '../topology/topology.service';
import { Endpoint } from '../models/endpoint.model';
import { Switch } from '../models/switch.model';

/*
 *  Management page component.
 */
@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {
  private cytoscape: cytoscape;
  private layout: any;
  private data: any = [];
  private endpoints: Endpoint[];
  private switches: Switch[];

  endpointCount: number;
  switchCount: number;

  virtualPorts: any;
  switchData: [any, any, Switch];

  constructor(private topologyService: TopologyService) { }

  ngOnInit() {
    this.cytoscape = cytoscape({
      container: document.getElementById('topology'),
      elements: this.data,
      layout: cs.main.layout,
      style: cs.main.style,
      minZoom: cs.main.minZoom,
      maxZoom: cs.main.maxZoom,
    });
    // Load switches.
    this.loadSwitches().then(() => {
      // Load endpoints.
      this.loadEndpoints().then(() => {
        // Load edges / links.
        this.loadLinks().then(() => {
          // Set & run the layout.
          this.layout = this.cytoscape.elements().layout(cs.main.layout);
          this.layout.run();
          // Load virtual ports.
          this.loadVirtualPorts();
        });
      });
    });
    // Switch click event.
    this.cytoscape.on('tap', '.of-switch', event => {
      const node = event.target;
      const nodes = node.connectedEdges().targets();
      const edges = node.connectedEdges();
      const switchData = this.switches.filter(sw => {
        return sw.id === node.data().id;
      })[0] || undefined;
      this.switchData = [nodes, edges, switchData];
    });
  }

  /*
   *  Loads switch nodes from the back-end.
   */
  private loadSwitches(): Promise<void> {
    return this.topologyService.loadSwitches()
      .map(switches => {
        this.switches = switches;
        const nodes = [];
        for (const sw of switches) {
          nodes.push({
            classes: 'of-switch',
            data: {
              id: sw.id
            },
            group: 'nodes'
          });
        }
        this.cytoscape.add(nodes);
        this.switchCount = switches.length;
      })
      .toPromise()
      .catch(error => { console.log(error); });
    ;
  }

  /*
   *  Loads endpoint nodes from the back-end.
   */
  private loadEndpoints(): Promise<void> {
    return this.topologyService.loadEndpoints()
      .map(endpoints => {
        this.endpoints = endpoints;
        const nodes = [];
        for (const ep of endpoints) {
          nodes.push({
            classes: 'endpoint',
            data: {
              id: ep.id
            },
            group: 'nodes'
          });
        }
        this.cytoscape.add(nodes);
        this.endpointCount = endpoints.length;
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

  /*
   *  Generates edges (links) between nodes.
   */
  private loadLinks(): Promise<void> {
    return this.topologyService.loadLinks()
      .map(links => {
        const edges = [];
        // Switch <-> Switch edges.
        for (const link of links[0]) {
          const switchId1 = this.findSwitchId(link.switchPortId1);
          const switchId2 = this.findSwitchId(link.switchPortId2);
          if (switchId1 !== undefined && switchId2 !== undefined) {
            edges.push({
              data: {
                id: link.id,
                source: switchId1,
                target: switchId2
              },
              group: 'edges'
            });
          }
        }
        // Switch <-> Endpoint edges.
        for (const link of links[1]) {
          const switchId = this.findSwitchId(link.switchPortId);
          if (switchId !== undefined) {
            edges.push({
              data: {
                id: switchId + '-' + link.endpointId,
                source: switchId,
                target: link.endpointId
              },
              group: 'edges'
            });
          }
        }
        this.cytoscape.add(edges);
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

  /*
   *  Loads virtual ports from the back-end.
   */
  private loadVirtualPorts() {
    return this.topologyService.loadVirtualPorts()
      .map(vPorts => {
        this.virtualPorts = vPorts;
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

  /*
   *  Returns a switch ID associated with a port ID.
   */
  private findSwitchId(portId: string): string {
    let matches = [];
    this.switches.forEach(sw => {
      matches = matches.concat(sw.ports.filter(port => {
        return port.id === portId;
      }))
    })
    return matches[0].switchId || undefined;
  }

  resetLayout() {
    this.layout = this.cytoscape.elements().layout(cs.main.layout);
    this.layout.run();
  }
}
