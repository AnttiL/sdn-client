import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { FormModule } from '../form/form.module';
import { TopologyModule } from '../topology/topology.module';
import { ManagementComponent } from './management.component';
import { TopologyService } from '../topology/topology.service';

/*
 *  Exports the management page component.
 */
@NgModule({
  imports: [
    CoreModule,
    FormModule,
    TopologyModule
  ],
  providers: [
    TopologyService
  ],
  declarations: [
    ManagementComponent
  ]
})
export class ManagementModule { }
