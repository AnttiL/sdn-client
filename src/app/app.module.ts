import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { IndexModule } from './index/index.module';
import { LoginModule } from './login/login.module';
import { ManagementModule } from './management/management.module';

/*
 *  Root application module.
 */
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    IndexModule,
    LoginModule,
    ManagementModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
