import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { FormModule } from '../form/form.module';
import { LoginComponent } from './login.component';

/*
 *  Exports the login page component.
 */
@NgModule({
  imports: [
    CoreModule,
    FormModule
  ],
  declarations: [
    LoginComponent
  ]
})
export class LoginModule { }
