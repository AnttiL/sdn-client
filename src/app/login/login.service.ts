import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import { HttpClientService } from '../core/http-client.service';

/*
 *  Login service for authenticating users.
 */
@Injectable()
export class LoginService {

  constructor(private http: HttpClientService) { }

  login(username, password): Observable<boolean> {
    return this.http.post('/api/auth/login', {
      username: username,
      password: password
    })
    .map((response: Response) => {
      const token = response.json && response.headers.get('X-Access-Token');
      if (token) {
        localStorage.setItem('user', JSON.stringify({
          id: response.json().id,
          name: response.json().name,
          token: token
        }));
        return true;
      }
      return false;
    });
  }
}
