import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { LoginService } from '../login/login.service';
import { SwitchFormComponent } from './switch-form/switch-form.component';
import { SwitchService } from './switch-form/switch.service';

/*
 *  Exports different HTML forms used in the application.
 */
@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [
    LoginFormComponent,
    SwitchFormComponent
  ],
  providers: [
    LoginService,
    SwitchService
  ],
  exports: [
    LoginFormComponent,
    SwitchFormComponent
  ]
})
export class FormModule { }
