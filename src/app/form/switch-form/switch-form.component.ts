import { Component, OnInit, Input } from '@angular/core';
import cytoscape from 'cytoscape';

import { CytoscapeConfig as cs } from '../../core/config/cytoscape.config';
import { SwitchService } from './switch.service';
import { Switch } from '../../models/switch.model';
import { VirtualPort } from '../../models/virtual-port.model';

@Component({
  selector: 'app-switch-form',
  templateUrl: './switch-form.component.html',
  styleUrls: ['./switch-form.component.css']
})
export class SwitchFormComponent implements OnInit {
  private cytoscape: cytoscape;
  private layout: any;
  private data: any = [];

  // Template models
  // TODO Implement model for physical port.
  switch: Switch;
  port: any;
  vPorts: VirtualPort[];
  vPort: VirtualPort;
  addNew: boolean;
  lldp: boolean;
  vlanId: number;

  // All virtual ports.
  @Input()
  public virtualPorts: VirtualPort[];

  /*
   *  Set switch and graph data.
   *  Triggered when user clicks a switch node.
   */
  @Input()
  set switchData(data: any) {
    if (data !== undefined && data.length > 0) {
      // Initialize cytoscape.
      this.cytoscape = cytoscape({
        container: document.getElementById('switch-topology'),
        elements: this.data,
        style: cs.sub.style,
        layout: cs.sub.layout,
        minZoom: cs.sub.minZoom,
        maxZoom: cs.sub.maxZoom,
      });

      const nodes = data[0] || undefined;
      const edges = data[1] || undefined;
      this.switch = data[2] || undefined;

      this.cytoscape.add(nodes);
      this.cytoscape.add(edges);

      if (this.switch.ports.length > 0) {
        this.port = this.switch.ports[0];
        this.vPorts = this.getVirtualPortByPortId(this.switch.ports[0].id);
        this.vPort = this.vPorts[0] || new VirtualPort;
      }

      setTimeout(() => {
        this.cytoscape.center();
      }, 250);

    } else if (this.cytoscape !== undefined) {
      this.cytoscape.destroy();
    }
  }

  constructor(private switchService: SwitchService) { }

  ngOnInit() { }

  close() {
    this.switchData = undefined;
    this.switch = undefined;
    this.port = undefined;
    this.cytoscape.destroy();
  }

  selectPort(i) {
    this.port = this.switch.ports[i] || undefined;
    this.vPorts = this.getVirtualPortByPortId(this.port.id) || [];
    this.vPort = this.vPorts[0] || new VirtualPort;
  }

  getVirtualPortByPortId(id) {
    return this.virtualPorts.filter(vp => {
      return vp.portId === id;
    });
  }

  changeVirtualPort(id) {
    this.vPort = this.virtualPorts.filter(vp => {
      return vp.id === id;
    })[0];
  }

  createVirtualPort(): Promise<void> {
    // TODO uuid binding
    return this.switchService.createVirtualPort(this.switch.id, this.port.id)
      .map(vp => {
        this.virtualPorts.push(vp);
        this.vPorts.push(vp);
        this.vPort = vp;
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

  saveVirtualPort() {
    if (this.vPort.reqPortConf === null) {
      this.vPort.reqPortConf = {};
    }
    if (this.vlanId !== undefined) {
      this.vPort.reqPortConf.vlan = this.vlanId;
    }
    if (this.lldp) {
      this.vPort.reqPortConf.lldp_listen = true;
      this.vPort.reqPortConf.lldp_send = true;
      this.vPort.reqPortConf.lldp_send_interval = 2;
    } else {
      delete this.vPort.reqPortConf.lldp_listen;
      delete this.vPort.reqPortConf.lldp_send;
      delete this.vPort.reqPortConf.lldp_send_interval;
    }
    return this.switchService.saveVirtualPort(this.vPort)
      .map(vp => {
        console.log(vp);
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

  deleteVirtualPort() {
    return this.switchService.deleteVirtualPort(this.vPort.id)
      .map(deleted => {
        this.vPorts = this.vPorts.filter(vp => {
          return vp.id !== deleted.id;
        });
        this.vPort = this.vPorts[0] || new VirtualPort;
      })
      .toPromise()
      .catch(error => { console.log(error); });
  }

}
