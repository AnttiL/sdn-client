import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import { HttpClientService } from '../../core/http-client.service';
import { VirtualPort } from '../../models/virtual-port.model';

@Injectable()
export class SwitchService {

  constructor(private http: HttpClientService) { }

  createVirtualPort(switchId, portId): Observable<VirtualPort> {
    const data = {
      reqMirroring: null,
      reqPortConf: null,
      reqPortState: 'DISABLED',
      reqPriority: 1,
      reqServiceConf: null,
      reqTraffMatch: null,
      vportNotes: {
        name: '',
        note: ''
      }
    };
    return this.http.post('/api/switches/' + switchId + '/ports/' + portId + '/vports', data)
      .map((response: Response) => {
        return response.json();
      });
  }

  saveVirtualPort(portConf): Observable<VirtualPort> {
    const data = {
      reqMirroring: portConf.reqMirroring,
      reqPortConf: portConf.reqPortConf,
      reqPortState: portConf.reqPortState,
      reqPriority: portConf.reqPriority,
      reqServiceConf: portConf.reqServiceConf,
      reqTraffMatch: portConf.reqTraffMatch,
      vportNotes: portConf.vportNotes
    };
    return this.http.patch('/api/vports/' + portConf.id, data)
      .map((response: Response) => {
        return response.json();
      });
  }

  /*
   *  Sends a RESTful delete request to delete a virtual port by ID.
   */
  deleteVirtualPort(portId): Observable<VirtualPort> {
    return this.http.delete('/api/vports/' + portId)
      .map((response: Response) => {
        return response.json();
      });
  }

}
