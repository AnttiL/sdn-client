import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from '../../login/login.service';

/*
 *  Login form component displayed on the login page.
 */
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  username = '';
  password = '';
  submitted = false;
  loginError = false;

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.loginError = false;
    this.submitted = true;
    this.loginService.login(this.username, this.password)
      .subscribe(
        result => {
          if (result === true) {
            this.router.navigate(['/management']);
          } else {
            this.submitted = false;
          }
        },
        error => {
          this.loginError = true;
          this.submitted = false;
        }
      );
  }
}
