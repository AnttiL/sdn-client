import { SdnClientPage } from './app.po';

describe('sdn-client App', () => {
  let page: SdnClientPage;

  beforeEach(() => {
    page = new SdnClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
